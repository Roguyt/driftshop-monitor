module.exports = {
    token: process.env.TELEGRAM_TOKEN || '',
    chatId: process.env.TELEGRAM_ID || '',
};
