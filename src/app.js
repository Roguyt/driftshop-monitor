const axios = require('axios');
const cheerio = require('cheerio');
const TelegramBot = require('node-telegram-bot-api');

const scriptConfig = require('./config/script.config');
const telegramConfig = require('./config/telegram.config');

(async () => {
    const bot = new TelegramBot(telegramConfig.token, { polling: true });

    bot.onText(/\/me/, (msg) => {
        bot.sendMessage(msg.chat.id, msg.chat.id);
    });

    await checkDriftshop(bot);
    setInterval(() => checkDriftshop(bot), 3600000);
})();

async function checkDriftshop(bot) {
    let $ = null;
    try {
        const html = (await axios.get(scriptConfig.urlToCheck)).data;
        $ = cheerio.load(html);
        const stock = $('strong.stockOk').html();

        if (stock == null) {
            console.log('No stock, checking again later');
            return;
        }

        const amount = stock.match(/\d+/)[0];

        await bot.sendMessage(
            telegramConfig.chatId,
            `Driftshop.fr got ${amount} back in stock.\nOrder them at ${scriptConfig.urlToCheck}`,
        );
    } catch (e) {
        console.error(e);
        await bot.sendMessage(telegramConfig.chatId, 'An error occured when loading Driftshop.fr');
    }
}
