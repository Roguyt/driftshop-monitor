module.exports = {
    parserOptions: {
        ecmaVersion: 2017,
    },
    plugins: ['simple-import-sort', 'import'],
    extends: ['eslint:recommended', 'prettier'],
    root: true,
    env: {
        node: true,
    },
    rules: {
        'sort-imports': 'off',
        'import/order': 'off',
        'simple-import-sort/imports': [
            'error',
            {
                groups: [
                    // Node stuff
                    [
                        '^(assert|buffer|child_process|cluster|console|constants|crypto|dgram|dns|domain|events|fs|http|https|module|net|os|path|punycode|querystring|readline|repl|stream|string_decoder|sys|timers|tls|tty|url|util|vm|zlib|freelist|v8|process|async_hooks|http2|perf_hooks)(/.*|$)',
                    ],
                    // Packages. `react` related packages come first.
                    ['^nest', '^@?\\w'],
                    // Internal packages.
                    // Anything else
                    ['^(@|@)(/.*|$)'],
                    // Side effect imports.
                    ['^\\u0000'],
                    // Parent imports. Put `..` last.
                    ['^\\.\\.(?!/?$)', '^\\.\\./?$'],
                    // Other relative imports. Put same-folder imports and `.` last.
                    ['^\\./(?=.*/)(?!/?$)', '^\\.(?!/?$)', '^\\./?$'],
                ],
            },
        ],
        'simple-import-sort/exports': 'error',
        'import/first': 'error',
        'import/newline-after-import': 'error',
        'import/no-duplicates': 'error',
    },
};
